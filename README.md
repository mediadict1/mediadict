**NOTE
For MediaDict to function smoothly, please ensure that your device is making  proper data connections.
This app has not been designed to fit multiple screen size, works best on  5.5 inch screen.**
 


MediaDict, a Next Generation Media Player for organizing and having fun with media be it in the form of music, movies or TV series.

* Listen to music according to the weather outside (Automatic weather based music suggestions).
* Follow/Unfollow your favourite celebrities and receive latest notifications about them automatically in the 'View Notifications' tab.
* Search for a song/movie/tv series either by an exact name or through a keyword and store the results in a database to be viewed later.Rate them yourself too.
* See the list of recent five performances of the artist of the track name you searched, thereby intelligently suggesting more of songs which you may like.
* Get the MediaDict rating of the movie through an efficient algorithm.
* Get the list of the best movies currently running in theaters, for you to choose the best among many.
* Added feature of Text To Speech converter, to listen to movie reviews rather than reading them.